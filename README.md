ManagedGcAwareDump Binaries repository
=====================================

### This repository was designed to host binaries with ManagedGcAwareDump tool. This repository doesn't contain Issue Tracker or Wiki. ###

#### Refer to the [https://bitbucket.org/zvirja/managedgcawaredump](https://bitbucket.org/zvirja/managedgcawaredump) repository to get [Wiki](https://bitbucket.org/zvirja/managedgcawaredump/wiki/Home) and [Issue Tracker](https://bitbucket.org/zvirja/managedgcawaredump/issues?status=new&status=open). ####

#### Refer to the [https://bitbucket.org/zvirja/managedgcawaredump-binaries/downloads](https://bitbucket.org/zvirja/managedgcawaredump-binaries/downloads) page to download the latest version of tool. ####